from django.shortcuts import render, redirect
from django.urls import reverse
from .forms import CreateActivity
from .models import Activity

# Create your views here.


def homepage(request):
    return render(request, 'index.html')


def blog(request):
    return render(request, 'blog.html')


def schedule(request):
    if request.method == 'POST':
        form = CreateActivity(request.POST)
        if form.is_valid():
            act = Activity(
                title=form.data["title"],
                date=form.data["date"],
                time=form.data["time"],
                location=form.data["location"],
                category=form.data["category"],
            )
            act.save()
            form = CreateActivity()

    else:
        form = CreateActivity()

    activities = Activity.objects.all()

    context = {
        'form': form,
        'activities': activities,
    }
    print(request.POST)
    return render(request, 'schedule.html', context)


def delete_activity(request):
    if request.method == 'POST' and 'id' in request.POST:
        Activity.objects.get(id=request.POST['id']).delete()
    return redirect(reverse('schedule'))
