from django import forms


class CreateActivity(forms.Form):
    title = forms.CharField(max_length=55)
    date = forms.DateTimeField(input_formats=['%Y-%m-%d'])
    time = forms.TimeField()
    location = forms.CharField(max_length=55)
    category = forms.CharField(max_length=30)

    date.widget.attrs.update({'autocomplete': 'off', 'placeholder': 'date'})
