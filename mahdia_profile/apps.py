from django.apps import AppConfig


class MahdiaProfileConfig(AppConfig):
    name = 'mahdia_profile'
