from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    url(r'^$', views.homepage, name='homepage'),
    url(r'^blog/$', views.blog, name='blog'),
    url(r'^schedule/$', views.schedule, name='schedule'),
    # path('delete/<int:id>', views.delete_activity, name='delete'),
    path('delete/', views.delete_activity, name='delete'),
]