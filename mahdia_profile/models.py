from django.db import models

# Create your models here.


class Activity(models.Model):
    title = models.CharField(max_length=55)
    date = models.DateField()
    time = models.TimeField()
    location = models.CharField(max_length=55)
    category = models.CharField(max_length=30)

    def __str__(self):
        return self.title
